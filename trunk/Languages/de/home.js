{
	"homepage" : "Homepage",
	
	"welcome" : "Willkommen bei Ace Attorney Online, dem kostenlosen Tool zum Erstellen von Ace-Attorney-Fällen!",
	"site_presentation" : "AAO ist der erste Ort, an dem man Fälle im Stil von Capcoms \"Ace Attorney\"-Reihe kreieren, spielen, teilen und diskutieren kann.",
	"no_download" : "Kein Download, keine umständlichen Installationen - registirere Dich und erstelle Deinen Fall ganz einfach im Browser!",
	
	"news" : "Letzte Neuigkeiten",
	"posted_on" : "Gepostet am <date> um <time>",
	"view_comments" : "Kommentare anzeigen",
	
	"random_featured" : "Herausragendes Spiel",
	
	"affiliates" : "Partner",
	"contact" : "Kontakt"
}
